package org.bw.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import org.bw.common.constant.MessageConstant;
import org.bw.common.constant.RedisConstant;
import org.bw.common.entity.PageResult;
import org.bw.common.entity.QueryPageBean;
import org.bw.common.entity.Result;
import org.bw.common.pojo.Setmeal;
import org.bw.service.SetmealService;
import org.bw.utils.QiniuUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import redis.clients.jedis.JedisPool;

import java.io.IOException;
import java.util.UUID;

/**
 * Created by fangjicai on 2020/2/25.
 */

@RestController
@RequestMapping("/setmeal")
public class SetmealController {

    @Reference
    private SetmealService setmealService;

    @Autowired
    private JedisPool jedisPool;

    @RequestMapping("/upload")
    public Result upload(@RequestParam("imgFile")MultipartFile imgFile){
        //获取原始名称
        String originalFilename = imgFile.getOriginalFilename();
        //获取最后一个.左右的字符串索引
        int lastIndexOf = originalFilename.lastIndexOf(".");
        //后去文件后缀
        String suffix = originalFilename.substring(lastIndexOf - 1);
        //生成随机文件名称 作为key
        String fileName = UUID.randomUUID().toString() + suffix;
        try {
            QiniuUtils.upload2Qiniu(imgFile.getBytes(),fileName);
            //将图片名称，也就是key存储在redis的set集合中农
            if(fileName!=null && fileName.length()>0){
                jedisPool.getResource().sadd(RedisConstant.SETMEAL_PIC_RESOURCES,fileName);
            }
            return new Result(true,MessageConstant.PIC_UPLOAD_SUCCESS,fileName);
        } catch (IOException e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.PIC_UPLOAD_FAIL);
        }
    }

    @RequestMapping("/add")
    public Result add(@RequestBody Setmeal setmeal,Integer[] checkgroupIds){

        try {
            this.setmealService.add1(setmeal,checkgroupIds);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.ADD_SETMEAL_FAIL);
        }
        return new Result(true,MessageConstant.ADD_SETMEAL_SUCCESS);

    }

    @PostMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
      PageResult pageResult = this.setmealService.findPage(queryPageBean.getCurrentPage(),queryPageBean.getPageSize(),
                queryPageBean.getQueryString());
      return pageResult;
    }


}

package org.bw.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import org.bw.common.constant.MessageConstant;
import org.bw.common.entity.PageResult;
import org.bw.common.entity.QueryPageBean;
import org.bw.common.entity.Result;
import org.bw.common.pojo.CheckItem;
import org.bw.service.CheckItemService;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by fangjicai on 2020/2/21.
 */
//组合注解 包含了@ResponseBody ,方法中不用添加了
@RestController
@RequestMapping("/checkitem")
public class CheckItemController {
    //此处远程引用服务提供端提供的服务
    @Reference
    private CheckItemService checkItemService;

    @RequestMapping("/add")
    public Result add(@RequestBody CheckItem checkItem){
        System.out.println("checkItem"+checkItem.toString());
        try {
            this.checkItemService.add(checkItem);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_CHECKGROUP_FAIL);
        }

        return new Result(true,MessageConstant.ADD_CHECKGROUP_SUCCESS);
    }

    /**
     * 分页查询
     * @param queryPageBean
     * @return
     */
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
     PageResult pageResult=   this.checkItemService.findPage(queryPageBean.getCurrentPage(),queryPageBean.getPageSize(),queryPageBean.getQueryString());
       return pageResult;
    }

    /**
     * 删除检查项
     * @param id
     * @return
     */
    @GetMapping("/delete")
    public Result delete(@RequestParam("id") Integer id){

        try {
            this.checkItemService.delete(id);
        }
        catch (RuntimeException e){
            return new Result(false,e.getMessage());
        }
        catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.DELETE_CHECKITEM_FAIL);
        }

        return new Result(true,MessageConstant.DELETE_CHECKITEM_SUCCESS);

    }


    //编辑
    @RequestMapping("/edit")
    public Result edit(@RequestBody CheckItem checkItem){

        try {
            this.checkItemService.edit(checkItem);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.EDIT_CHECKITEM_FAIL);
        }
        return new Result(true,MessageConstant.EDIT_CHECKITEM_SUCCESS);
    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @RequestMapping("/findById")
    public Result findById(Integer id){

        try {
          CheckItem checkItem=  this.checkItemService.findById(id);
          return new Result(true,MessageConstant.QUERY_CHECKITEM_SUCCESS,checkItem);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.QUERY_CHECKITEM_SUCCESS);
        }

    }

    /**
     * 获取所有的检查项
     * @return
     */
    @GetMapping("/findAll")
    public Result findAll(){
     List<CheckItem> checkItemList=   this.checkItemService.findAll();
     if(!CollectionUtils.isEmpty(checkItemList)){
         return new Result(true,MessageConstant.QUERY_CHECKITEM_SUCCESS,checkItemList);
     }
     return new Result(false,MessageConstant.QUERY_CHECKITEM_FAIL);
    }



}

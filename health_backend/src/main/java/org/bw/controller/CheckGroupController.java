package org.bw.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import org.bw.common.constant.MessageConstant;
import org.bw.common.entity.PageResult;
import org.bw.common.entity.QueryPageBean;
import org.bw.common.entity.Result;
import org.bw.common.pojo.CheckGroup;
import org.bw.service.CheckGroupService;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by fangjicai on 2020/2/24.
 */

@RestController
@RequestMapping("/checkgroup")
public class CheckGroupController {

    @Reference
    private CheckGroupService checkGroupService;

    @RequestMapping("/add")
    public Result add(@RequestBody CheckGroup checkGroup,Integer[] checkitemIds){

        try {
            this.checkGroupService.add(checkGroup,checkitemIds);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_CHECKGROUP_FAIL);
        }
        return new Result(true,MessageConstant.ADD_CHECKGROUP_SUCCESS);
    }

    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
     PageResult pageResult=this.checkGroupService.pageQuery(queryPageBean.getCurrentPage(),
                queryPageBean.getPageSize(),queryPageBean.getQueryString());
     return pageResult;
    }

    //根据id查询
    @RequestMapping("/findById")
    public Result findById(Integer id){
     CheckGroup checkGroup= this.checkGroupService.findById(id);
     if(checkGroup!= null){
         return new Result(true,MessageConstant.QUERY_CHECKGROUP_SUCCESS,checkGroup);
     }
     return new Result(false,MessageConstant.QUERY_CHECKGROUP_FAIL);
    }

    //根据查询组id查询对应的检查项checkitem ids
    @RequestMapping("/findCheckItemIdsByCheckGroupId")
    public Result findCheckItemIdsByCheckGroupId(Integer id){
      List<Integer> checkitemIds=  this.checkGroupService.findCheckItemIdsByCheckGroupId(id);
      if (CollectionUtils.isEmpty(checkitemIds)){
          return new Result(false,MessageConstant.QUERY_CHECKITEM_FAIL);
      }
      return new Result(true,MessageConstant.QUERY_CHECKITEM_SUCCESS,checkitemIds);
    }
    //编辑
    @RequestMapping("/edit")
    public Result edit(@RequestBody CheckGroup checkGroup,Integer[] checkitemIds){

        try {
            this.checkGroupService.edit(checkGroup,checkitemIds);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.EDIT_CHECKGROUP_FAIL);
        }
        return new Result(true,MessageConstant.EDIT_CHECKGROUP_SUCCESS);
    }

    //根据id删除
    @GetMapping("/delete")
    public Result delete(Integer id){
        try {
            this.checkGroupService.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.DELETE_CHECKGROUP_FAIL);
        }
        return new Result(true,MessageConstant.DELETE_CHECKGROUP_SUCCESS);

    }

    //查询所有分组
    @GetMapping("/findAll")
    public Result findAll(){
        List<CheckGroup> checkGroups= this.checkGroupService.findAll();
        if(CollectionUtils.isEmpty(checkGroups)){
            return new Result(false,MessageConstant.QUERY_CHECKGROUP_FAIL);
        }
        return new Result(true,MessageConstant.QUERY_CHECKGROUP_SUCCESS,checkGroups);
    }

}

package org.bw.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.bw.common.entity.PageResult;
import org.bw.common.pojo.CheckGroup;
import org.bw.dao.CheckGroupDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by fangjicai on 2020/2/24.
 */

@Service(interfaceClass = CheckGroupService.class)
public class CheckGroupServiceImpl implements CheckGroupService {
    @Autowired
    private CheckGroupDao checkGroupDao;

    @Override
    @Transactional
    public void add(CheckGroup checkGroup, Integer[] checkitemIds) {
        //第一步 插入checkgroup主信息，并获取id
        checkGroupDao.add(checkGroup);
        //第二步 向checkitme_checkGroup中间表插入数据
        this.setCheckGroupAndCheckItem(checkGroup.getId(),checkitemIds);
    }

    @Override
    public PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString) {
        PageHelper.startPage(currentPage,pageSize);
        Page<CheckGroup> page= this.checkGroupDao.selectByConditon(queryString);
        return new PageResult(page.getTotal(),page.getResult());
    }

    @Override
    public CheckGroup findById(Integer id) {
        return this.checkGroupDao.findById(id);
    }

    @Override
    public List<Integer> findCheckItemIdsByCheckGroupId(Integer id) {
     List<Integer> checkItemIds= this.checkGroupDao.findCheckItemIdsByCheckGroupId(id);
        return checkItemIds;
    }

    @Override
    @Transactional
    public void edit(CheckGroup checkGroup, Integer[] checkitemIds) {
        //更新主表
        this.checkGroupDao.edit(checkGroup);
        //删除主表id关联的checkitem
        this.checkGroupDao.deleteAssociation(checkGroup.getId());
        //插入最新的更新数据
        this.setCheckGroupAndCheckItem(checkGroup.getId(),checkitemIds);
    }

    @Override
    public void deleteById(Integer id) {
        this.checkGroupDao.deleteById(id);
    }

    public void setCheckGroupAndCheckItem(Integer checkGroupId,Integer[] checkItemIds){
        if(!CollectionUtils.isEmpty(Arrays.asList(checkItemIds))){
            for (Integer checkItemId : checkItemIds) {
                HashMap<String, Integer> map = new HashMap<>();
                map.put("checkgroup_id",checkGroupId);
                map.put("checkitem_id",checkItemId);
                checkGroupDao.setCheckGroupAndCheckItem(map);
            }
        }
    }

    @Override
    public List<CheckGroup> findAll() {
        return this.checkGroupDao.findAll();
    }
}

package org.bw.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.bw.common.entity.PageResult;
import org.bw.common.pojo.CheckItem;
import org.bw.dao.CheckItemDao;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by fangjicai on 2020/2/21.
 */
//因为一个类可以实现多个接口，所有此处可指定注册到zookeeper的到底是哪个接口
@Service(interfaceClass = CheckItemService.class)
public class CheckItemServiceImpl implements CheckItemService {
    @Autowired
    private CheckItemDao checkItemDao;

    @Override
    public void add(CheckItem checkItem) {
        checkItemDao.add(checkItem);

    }

    @Override
    public PageResult findPage(Integer currentPage, Integer pageSize, String queryString) {
        PageHelper.startPage(currentPage,pageSize);
        Page<CheckItem> page= this.checkItemDao.selectByConditon(queryString);
        return new PageResult(page.getTotal(),page.getResult());
    }

    @Override
    public void delete(Integer id) {
        //检查id对应的检查项是否关联检查组
      long count= this.checkItemDao.findCountByCheckItemId(id);
      if(count>0){
          throw  new RuntimeException("当前项被引用，不能删除");
      }
      this.checkItemDao.deleteById(id);

    }

    @Override
    public CheckItem findById(Integer id) {
      CheckItem checkItem=  this.checkItemDao.findById(id);
        return checkItem;
    }

    @Override
    public void edit(CheckItem checkItem) {
        this.checkItemDao.edit(checkItem);
    }

    @Override
    public List<CheckItem> findAll() {
        return this.checkItemDao.findAll();
    }
}

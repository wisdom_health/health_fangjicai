package org.bw.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.microsoft.schemas.office.visio.x2012.main.impl.PagesDocumentImpl;
import org.apache.poi.hpsf.ReadingNotSupportedException;
import org.bw.common.constant.RedisConstant;
import org.bw.common.entity.PageResult;
import org.bw.common.pojo.Setmeal;
import org.bw.dao.SetmealDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import redis.clients.jedis.JedisPool;

import javax.print.attribute.standard.PagesPerMinute;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by fangjicai on 2020/2/26.
 */

@Service(interfaceClass = SetmealService.class )
public class SetmealServiceImpl implements SetmealService {
    @Autowired
    private SetmealDao setmealDao;

    @Autowired
    private JedisPool jedisPool;


    @Override
    @Transactional
    public void add1(Setmeal setmeal, Integer[] checkgroupIds) {
        this.setmealDao.add(setmeal);
        //针对保存操作，redis也要维护一个set集合，一个是上传图片的集合，一个是最终保存的集合，两个集合的差集就是所谓
        //垃圾图片
        //为什么选择set集合？set集群有去重的功能
        if(setmeal.getImg()!=null && setmeal.getImg().length()>0){
            jedisPool.getResource().sadd(RedisConstant.SETMEAL_PIC_DB_RESOURCES,setmeal.getImg());
        }

        if(!CollectionUtils.isEmpty(Arrays.asList(checkgroupIds))){
            setSetmealAndCheckgroup(setmeal.getId(),checkgroupIds);
        }


    }

    @Override
    public PageResult findPage(Integer currentPage, Integer pageSize, String queryString) {
        PageHelper.startPage(currentPage, pageSize);
        Page<Setmeal> page= this.setmealDao.queryByConditon(queryString);
        return new PageResult(page.getTotal(),page.getResult());
    }

    private void setSetmealAndCheckgroup(Integer id, Integer[] checkgroupIds) {
        for (Integer checkgroupId : checkgroupIds) {
            HashMap<String, Integer> map = new HashMap<>();
            map.put("setmeal_id",id);
            map.put("checkgroup_id",checkgroupId);
            this.setmealDao.setSetmealAndCheckgroup(map);
        }
    }
}

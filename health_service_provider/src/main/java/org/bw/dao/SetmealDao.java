package org.bw.dao;

import com.github.pagehelper.Page;
import org.bw.common.pojo.Setmeal;

import java.util.HashMap;

/**
 * Created by fangjicai on 2020/2/26.
 */
public interface SetmealDao {
    public void add(Setmeal setmeal);

    public void setSetmealAndCheckgroup(HashMap<String, Integer> map) ;

    Page<Setmeal> queryByConditon(String queryString);
}

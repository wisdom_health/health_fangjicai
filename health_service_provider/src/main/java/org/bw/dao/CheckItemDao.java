package org.bw.dao;

import com.github.pagehelper.Page;
import org.bw.common.pojo.CheckItem;

import java.util.List;

/**
 * Created by fangjicai on 2020/2/21.
 */
public interface CheckItemDao {
  //添加
  public void add(CheckItem checkItem);

  //根据字符串模糊查询
  public Page<CheckItem> selectByConditon(String queryString);

  long findCountByCheckItemId(Integer id);

  void deleteById(Integer id);

    CheckItem findById(Integer id);

  void edit(CheckItem checkItem);

  List<CheckItem> findAll();
}

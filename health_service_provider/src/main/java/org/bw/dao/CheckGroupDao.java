package org.bw.dao;

import com.github.pagehelper.Page;
import org.bw.common.pojo.CheckGroup;

import java.util.HashMap;
import java.util.List;

/**
 * Created by fangjicai on 2020/2/24.
 */
public interface CheckGroupDao {
    public void add(CheckGroup checkGroup);

    public void setCheckGroupAndCheckItem(HashMap<String, Integer> map);

    Page<CheckGroup> selectByConditon(String queryString);

    //更新操作
    void edit(CheckGroup checkGroup);

    //根据groupid删除 group-checktiem中间表数据
    void deleteAssociation(Integer id);

    //根据主键id查询
    CheckGroup findById(Integer id);

    //根据groupid 查询checkitem的id集合
    List<Integer> findCheckItemIdsByCheckGroupId(Integer id);

    void deleteById(Integer id);

    List<CheckGroup> findAll();
}

package org.bw.service;

import org.bw.common.entity.PageResult;
import org.bw.common.pojo.CheckGroup;

import java.util.List;

/**
 * Created by fangjicai on 2020/2/24.
 */
public interface CheckGroupService {
    void add(CheckGroup checkGroup, Integer[] checkitemIds);

    PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString);

    CheckGroup findById(Integer id);

    List<Integer> findCheckItemIdsByCheckGroupId(Integer id);

    void edit(CheckGroup checkGroup, Integer[] checkitemIds);

    void deleteById(Integer id);

   public List<CheckGroup> findAll();
}

package org.bw.service;

import org.bw.common.entity.PageResult;
import org.bw.common.pojo.Setmeal;

/**
 * Created by fangjicai on 2020/2/25.
 */
public interface SetmealService {
  public void add1(Setmeal setmeal, Integer[] checkgroupIds);

    PageResult findPage(Integer currentPage, Integer currentPage1, String queryString);
}

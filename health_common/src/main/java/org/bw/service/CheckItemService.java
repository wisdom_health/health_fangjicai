package org.bw.service;

import org.bw.common.entity.PageResult;
import org.bw.common.pojo.CheckItem;

import java.util.List;

/**
 * Created by fangjicai on 2020/2/21.
 */
public interface CheckItemService {
   public void add(CheckItem checkItem);

  public PageResult findPage(Integer currentPage, Integer pageSize, String queryString);

  public void delete(Integer id);

  public  CheckItem findById(Integer id);

  public  void edit(CheckItem checkItem);

    List<CheckItem> findAll();
}

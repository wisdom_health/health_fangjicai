package org.bw.jobs;

import org.bw.common.constant.RedisConstant;
import org.bw.utils.QiniuUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import redis.clients.jedis.JedisPool;

import java.util.Set;

/**
 * Created by fangjicai on 2020/2/26.
 */
public class ClearImgJob {

    @Autowired
    private JedisPool jedisPool;

    public void clearImg(){
        //求redis的两个set集合的差值 diffenrent SETMEAL_PIC_RESOURCES>=SETMEAL_PIC_DB_RESOURCES
        Set<String> set = jedisPool.getResource().
                sdiff(RedisConstant.SETMEAL_PIC_RESOURCES, RedisConstant.SETMEAL_PIC_DB_RESOURCES);
        //判断集合是否为空
        if(!CollectionUtils.isEmpty(set)){
            for (String key : set) {
                //在七牛云中删除响应的图片
                QiniuUtils.deleteFileFromQiniu(key);
                jedisPool.getResource().srem(RedisConstant.SETMEAL_PIC_RESOURCES,key);
            }
        }
        //从七牛云中将差值对应的图片对象删除


    }
}
